import axios from "axios";

export default axios.create({
  baseURL: "https://api.unsplash.com/",
  headers: {
    Authorization:
      "Client-ID 47166ad0f643c80d0d6ccf1ed1d1704f9bbac5425858ef061cac89d9fbab3ef4"
  }
});
